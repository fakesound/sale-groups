## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost
npm run serve

# development build without minification
npm run dev

# build for production with minification
npm run build
```
