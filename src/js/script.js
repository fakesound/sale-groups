var $ = jQuery = window.jQuery = require('jquery')
    fancybox = require('@fancyapps/fancybox'),
    select2 = require('select2/dist/js/select2.full.js')($);

$(document).ready(function(){
    $('[data-fees]').hover(function(){
        $('[data-fees="' + $(this).data('fees') + '"]').addClass('active');
    }, function(){
        $('[data-fees="' + $(this).data('fees') + '"]').removeClass('active');
    });

    $(".calculate .select2").each(function(){
        var $this = $(this),
            $parent = $this.parents('.calculate');;

        $this.select2({
            width: 'resolve',
            minimumResultsForSearch: -1,
            containerCssClass: 'calculate__select-container',
            dropdownCssClass: 'calculate__select-dropdown',
            dropdownParent: $parent,
        });
    });


    $("[data-fancybox]").fancybox({
		afterShow: function( instance, slide ) {

            slide.$slide.find(".select2").each(function(){
                var $this = $(this),
                    $parent = $this.parents('.deal-popup');
        
                $this.select2({
                    minimumResultsForSearch: -1,
                    containerCssClass: 'deal-popup__select-container container',
                    dropdownCssClass: 'deal-popup__select-dropdown dropdown',
                    dropdownParent: $parent,
                });
            });
		}
	});
});